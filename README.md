# ENVOL 2021 TP Angular

## Introduction

Dêpôt source contenant le TP utilisé pour la formation ENVOL 2021. Le guide est écrit en utilisant l'outil mkdocs (https://www.mkdocs.org/).

## Installation

Vous aurez besoin d'une image docker local contenant l'outil mkdocs pour utiliser ce dépôt.

 - Pour générer l'image : `make install`

## Lancer le serveur de développement

MkDocs fournit un serveur de développement qui vous permet de visualiser votre documentation et qui rafraichit la page à chaque changement de fichiers.

 - Pour lancer le serveur : `make start`

Après le lancement l'application est disponible sur : http://localhost:8888

## Générer le site web

Le site web est généré dans un dossier nommé `site`

 - Pour lancer la génération : `make build`

## Auteurs

Ce guide a été realisé par : 

 - François Agneray : Laboratoire d'Astrophysique de Marseille
 - Tifenn Guillas : Laboratoire d'Astrophysique de Marseille
