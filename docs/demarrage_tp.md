# Démarrage du TP

Dans cette partie nous allons : 

 - Cloner le dépôt permettant de commencer le TP
 - Installer les dépendances
 - Démarrer l'application
 - Expliquer l'arborescence du projet
 - Lister et expliquer les commandes disponibles

## Récupération du dépôt

Pour pouvoir démarrer le TP, nous allons cloner le dépôt git contenant les fichiers sources de base : [https://gitlab.lam.fr/envol/envol-2021-tp-angular.git](https://gitlab.lam.fr/envol/envol-2021-tp-angular.git)

```sh
git clone https://gitlab.lam.fr/envol/envol-2021-tp-angular.git
```

À chaque étape du TP vous pourrez retrouver une branche correspondante et dans la branche `master` vous pouvez retrouver l'application finalisée.

Pour vérifier que vous êtes bien dans la branche démarrage : 

```sh
cd envol-2021-tp-angular
git branch
```

Pour voir toutes les branches disponibles sur le dépôt distant : 

```sh
git branch -ar
```

Pour récuperer une branche particulière, par exemple `les_bases` :

```sh
git fetch origin les_bases:les_bases
```

Pour vous déplacer sur une branche :

```sh
git checkout les_bases
```

**Attention** : Assurez-vous de bien vous replacer sur la branche `demarrage` pour continuer

## Installation des dépendances

Il faut maintenant installer les dépendances du projet. La commande `make install` est un raccourci qui permet de faciliter cette action.

```sh
make install
```

Les dépendances sont téléchargées et installées dans le dossier `client/node_modules`.

Voici les dépendances utilisées dans ce projet : 

- [Angular v12](https://angular.io/) : Le framework principal de l'application
- [Bootstrap 5](https://getbootstrap.com/) : Feuille de style CSS pour les formulaires, boutons, outils de navigation, ect...
- [ngx-bootstrap](https://valor-software.com/ngx-bootstrap/) : Ensemble de components utilisant Bootstrap 5 et directement intégré à l'univers Angular
- [ngx-toastr](https://github.com/scttcper/ngx-toastr) : Module qui permet de lancer des notifications à l'utilisateur sous forme de toasts
- [ngx-charts](https://swimlane.gitbook.io/ngx-charts/) : Bibliothèque qui permet de générer des graphiques (barre, secteur, ect...)
- [Font Awesome](https://fontawesome.com/) : Bibliothèque d'icones vectorielles, libre d'utilisation
- [Jest](https://jestjs.io/) : Framework de test Javascript que nous utiliserons

## Démarrage de l'application

Nous pouvons maintenant démarrer l'application. Angular nécessite un serveur de développement pour fonctionner. La commande `make start` permet de faciliter cette action.

```sh
make start
```

Vous pouvez accéder à la version en développement à l'adresse suivante : [http://localhost:4200](http://localhost:4200)

**Info** : La commande make start utilise Angular CLI et la commande `ng start` pour fonctionner.

## Arborescence du projet 

Nous allons ici détailler ensemble l'arborescence du projet.<br>
À la racine vous allez trouver : 

 - Le fichier `Makefile` qui permet de lancer des commandes sur le projet. Nous détaillerons plus loin la liste de ces commandes.
 - Le fichier `docker-compose.yml` qui permet le lancement des services nécessaires au projet via la commande docker.
 - Le dossier `client` qui contient le programme Angular (front-end)

Dans le dossier `client` vous allez trouver, entre autres : 

 - Le dossier `src` qui contient les fichiers sources typescript du projet. C'est dans ce dossier que vous allez travailler pour ajouter des modules, des components, des services, etc.
 - Le dossier `node_modules` qui contient les dépendances de l'application (framework Angular, bootstrap, ...). Ce dossier est généré automatiquement via la commande `make install`
 - Le fichier `package.json` qui liste les dépendances nécessaires au projet

## Commandes make disponibles

Nous avons programmé des commandes pour vous aider à effectuer certaines opérations sur le projet. Vous pouvez lister les commandes disponibles en tapant simplement `make` dans un terminal à la racine du projet. Voici le type de commandes que vous allez pouvoir retrouver : 

 - `start`, `restart`, `stop` : lancer, relancer, stopper les services.
 - `status` : afficher le status des services.
 - `logs` : afficher les journaux.
 - `build` : construire l'application pour la production. Cette commande va créer le site web (html, css, js) qui sera disponible dans le dossier `client/dist`.
 - `tests`, `tests-wc` : exécuter les tests avec ou sans la couverture de code.
