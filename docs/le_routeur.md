# Le routeur

Dans cette partie nous allons apprendre à utiliser le routeur pour naviguer entre des pages.

Une application Angular est une `Single Page Application` et cela signifie que l'application est chargée entièrement au démarrage.
Cela n'empêche pas le développeur de vouloir découper son application en plusieurs pages et pour ce faire il faut utiliser le routeur.

## Les routes

Pour utiliser le routeur il faut d'abord créer un fichier de routing dans votre module puis déterminer des routes. 
Dans notre application de test ce fichier de routing existe déjà, c'est le fichier `app.routing.ts`.
Ce fichier de routing est déclaré dans le module principal `AppComponent` dans le tableau des imports.

La route où le visiteur sera dirigé est spécifiée dans l'URL.
Par exemple si l'URL est `http://localhost:4200/accueil` l'utilisateur sera dirigé sur la route `accueil`.
Si l'utilisateur rafraichît la page, l'application démarrera directement sur la page `accueil`.
L'utilisateur a donc l'impression de naviguer sur un site classique.

Exemple de routes que vous pouvez spécifier dans un fichier de routing : 

```ts
const routes: Routes = [
    { path: '', redirectTo: 'accueil', pathMatch: 'full' },
    { path: 'accueil', component: AccueilComponent },
    { path: 'reservations', component: ReservationsComponent },
    { path: '**', component: NotFoundPageComponent }
];
```

Explications :
 - La première ligne sert à indiquer quel route prendre si aucune n'est précisée dans l'URL. Ici le visiteur sera redirigé sur la route `accueil`
 - La route `accueil` va afficher ce qui est prévu par le component `AccueilComponent`
 - La route `reservations` va afficher ce qui est prévu par le component `ReservationsComponent`
 - La dernière ligne précise que si la route demandée par l'utilisateur ne match avec aucune des précédentes, c'est alors le component `NotFoundPageComponent` qui va s'afficher.

## Le router outlet

Le component spécifié par la route sera chargé à la place du component `<router-outlet></router-outlet>`. Dans notre exemple ce component doit donc être déclaré dans le component principal `AppComponent`.

Exemple du fichier `app.component.html` : 

```html
<h1>{{ title }}</h1>

<router-outlet></router-outlet>
```

Par exemple si le visiteur arrive sur l'URL `http://localhost:4200/accueil`, le component `AccueilComponent` sera affiché à la place de `<router-outlet></router-outlet>`.

## Changer de page

Vous pouvez créer des liens sur votre application pour permettre au visiteur de changer de page grâce à la directive `RouterLink`.
Pour l'utiliser vous devez créer un ligne sur votre page en appelant la directive avec comme paramètre l'URL à appeler.

Exemple :

```html
<a routerLink="reservations">Aller aux réservations</a>
```

Vous pouvez également demander le changement de page depuis le code TypeScript d'un component.
Il faut pour cela importer le service `Router` dans le constructeur de la classe puis appeler la méthode `navigate`.

Exemple :

```ts
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    constructor(private router: Router) { }

    myFunction() {
        this.router.navigate(['reservations']);
    }
}
```

## TP : Ajouter des routes

 1. Créer les components `HomeComponent` et `SuperHeroesComponent`
 2. Ajouter une nouvelle route `home` qui dirigera le visiteur vers `HomeComponent`
 3. Si le visiteur ne spécifie aucune route il doit être redirigé vers la route `home`
 4. Ajouter également une nouvelle route `super-heroes` qui dirigera vers `SuperHeroesComponent` 
 5. Ajouter une barre de menu dans `AppComponent` pour permettre au visiteur de naviguer entre vos deux pages
 6. Déplacer le code qui concerne la page d'accueil dans le component `HomeComponent`
 7. Déplacer le code qui concerne les super héros dans le component `SuperHeroesComponent`
 8. Améliorer le design de l'application avec Bootstrap

**Résultat attendu page home** : 

![le_routeur_s1](img/6_le_routeur_s1.png#center)

**Résultat attendu page super-heroes** : 

![le_routeur_s2](img/6_le_routeur_s2.png#center)

## Conclusion

Dans cette partie vous avez appris à fabriquer des pages dans votre application grâce au module `router`.