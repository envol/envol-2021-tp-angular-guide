# TP Angular n°1

Nous allons mettre en œuvre dans ce TP :

 - L'installation d'un environnement de développement d'application web en Angular
 - La prise en main du framework avec des exemples simples
 - Un exemple plus poussé à travers un formulaire de sélection de départements et de communes
 - La mise en forme des données sous forme de tableau et de graphique

## Déroulement du TP

Les données traitées seront tout d'abord statiques (table HTML), puis dynamiques provenant de web services.

Nous allons ensuite visualiser ces données sous forme d'un tableau et d'un histogramme.<br>
Nous rendrons notre interface interactive avec l'intégration d'un formulaire de recherche.

Enfin nous verrons comment compiler et générer l'application pour la mise en production.

## Guide

À chaque étape du TP, vous serez guidés pour la mise en oeuvre de votre application avec des indications et vous pourrez visualiser le résultat attendu sous forme de screenshot.

# Auteurs

Ce guide a été réalisé par : 

 - `François Agneray` : Laboratoire d'Astrophysique de Marseille
 - `Tifenn Guillas` : Laboratoire d'Astrophysique de Marseille
 