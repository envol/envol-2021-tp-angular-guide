# Les bases d'Angular

Dans cette partie nous allons : 

 - Revoir la notion de component
 - Expliquer le fonctionnement du component `AppComponent`
 - Modifier ce component pour intégrer notre premiere page
 - Afficher des données provenant d'une propriété du component

## Les components

Une des notions fondamentale est qu'une application Angular est composée d'une multitude de components.
Un component peut être appelé par un ou plusieurs autres components.

Un component est défini par une classe **TypeScript**.
Il est codé dans un fichier `.ts`. 
Il est accompagné d'un template `html` (la vue) ainsi que d'une feuille de style `css`.

## AppComponent

Pour chaque application il existe un component racine qui est appelé le `AppComponent`.
C'est le `root component`. Sa particularité est qu'il est unique et est appelé au démarrage de l'application. Il est donc obligatoire.

Ce component est déjà présent dans notre dépôt et vous pouvez le trouver dans le répertoire `client/src/app` sous la forme de trois fichiers : 

 - `app.component.ts` : Le fichier qui contient la classe TypeScript du component
 - `app.component.html` : Le fichier qui contient la vue au format html
 - `app.component.scss` : Le fichier qui contient le style CSS qui sera appliqué à ce component

## TP : L'interpolation

Pour afficher une propriété d'un component dans la vue vous devez utiliser le mécanisme de l'interpolation, comme ceci : `{{ property }}`.
L'interpolation permet de récupérer la valeur d'une propriété d'un component pour l'injecter dans la vue HTML. De plus Angular met à jour l’affichage dans le template si la propriété du composant est modifiée.

**Exercice** :

 1. Modifier la propriété `title` du component AppComponent par `Bienvenue dans le TP Angular ENVOL 2021`
 2. Effacer le contenu de la vue html puis ajouter un titre `h1` en affichant la propriété `title`

**Résultat attendu** : 

![premier_component_s1](img/3_premier_component_s1.png#center)

## TP : Afficher un tableau de valeurs

Vous allez dans certains cas avoir besoin d'afficher les valeurs d'un tableau. Pour boucler sur un tableau vous devez utiliser la directive structurelle `*ngFor`. Vous devez l'appeler sur la balise à répéter, comme ceci : 

```html
<li *ngFor="let property of list">{{ property }}</li>
```

Vous pouvez ensuite utiliser l'interpolation pour afficher la valeur de la variable récupérée à chaque tour de boucle.

**Exercice** :

 1. Ajouter une propriété `superheroes` dans le component AppComponent.
 2. Initialiser cette propriété avec la liste de super-héros suivant : `Hulk`, `Spider-Man`, `Thor`, `Iron Man`
 3. Afficher la propriété `superheroes` sous forme d'une liste html avec les balises `ul` et `li`

**Résultat attendu** : 

![premier_component_s2](img/3_premier_component_s2.png#center)

## TP : Tester une valeur

Vous pouvez tester la valeur d'une variable grâce à la directive structurelle `*ngIf`. Vous devez l'appeler sur la balise qui devra s'afficher si la condition est remplie, comme ceci : 

```html
<p *ngIf="property === 'value'">Bonjour le monde !<p>
```

**Exercice** :

 1. Ajouter l'icone font-awesome check avant le nom du super héro : `<span class="fas fa-check"></span>`
 2. Ajouter une condition pour n'afficher cette icône que si le nom du super héro est Hulk

**Résultat attendu** : 

![premier_component_s3](img/3_premier_component_s3.png#center)

## TP : Les évènements

Avec Angular le développeur peut déclencher des actions sur des évènements qui surviennent sur la page comme un clic sur un bouton.
Les évènements peuvent être écoutés, en utilisant les parenthèses, et on peut appeler une fonction du component lors du déclenchement, comme ceci :

```html
<button type="button" (click)="action()">Action</button>
```

**Exercice** :

 1. Ajouter une nouvelle propriété `newHero` dans le component `AppComponent` avec comme valeur `Black Widow`
 2. Ajouter une fonction `addNewHero` dans le component `AppComponent` qui va ajouter le nouveau super héro à la liste des héros s'il n'existe pas
 3. Ajouter un bouton dans la vue avec la valeur `Ajouter un nouveau héro` et qui appellera la fonction `addNewHero` au clic de l'utilisateur

**Résultat attendu (après le clic)** : 

![premier_component_s4](img/3_premier_component_s4.png#center)

## TP : Property binding

Nous avons vu que l'interpolation permet d'afficher la valeur d'une variable dans la vue HTML. Le property binding permet quant à lui de modifier la valeur d'une propriété d'un élément du DOM et ceci grâce au crochet. Nous pouvons par exemple modifier la valeur de la propriété `disabled` d'un bouton en fonction de la valeur d'une variable, comme ceci : 

```html
<button type="button" [disabled]="myValue" (click)="action()">Action</button>
```

**Exercice** :

 1. Ajouter une nouvelle propriété `newHeroAdded` dans le component `AppComponent` avec comme valeur `false`
 2. Désactiver le bouton `Ajouter un nouveau héro` si la valeur de la propriété `newHeroAdded` est `true`
 3. Modifier la fonction `addNewHero` pour passer la propriété `newHeroAdded` à `true`
 4. Dans la fonction `addNewHero` vous pouvez donc supprimer la vérification de l'existence du nouveau super héro dans la liste des héros puisque la fonction ne peut être appelée qu'une seule fois.

**Résultat attendu (après le clic)** : 

![premier_component_s5](img/3_premier_component_s5.png#center)

## TP : CSS class binding

Nous pouvons également appliquer une class CSS suivant une condition. Il faut noter que les classes CSS définies pour un component ne sont valables que pour ce component.
Pour pouvoir ajouter une classe CSS suivant une condition il faut utiliser le `property binding` avec comme nom `class` suivi du nom de la classe à appliquer. Pour appliquer la classe `.myClass` vous devrez donc faire `[class.myClass]`, comme ceci : 

```html
<p [class.myClass]="condition">Bonjour le monde !<p>
```

**Exercice** :

 1. Ajouter une classe CSS dans la feuille de style liée au component `AppComponent` que vous appellerez `isImportant`
 2. Dans la classe CSS ajouter l'instruction pour mettre en gras le texte
 3. Appliquer cette classe CSS dans la liste des héros que si le nom du super héro est égale à `Thor`

**Résultat attendu** : 

![premier_component_s6](img/3_premier_component_s6.png#center)

## Conclusion

Dans cette partie vous avez appris les premières commandes d'Angular, notamment : 

 - Afficher une propriété grâce à l'interpolation : `{{ myProperty }}`
 - Afficher une liste grâce à la directive `*ngFor`
 - Utiliser une expression conditionnelle côté template avec la directive `*ngIf`
 - Relier une fonction à un évènement avec les parenthèses : `(click)="myFunction()"`
 - Modifier la propriété d'un élément HTML avec les crochets : `[disabled]="value"`
 - Ajouter une classe sur un élément en fonction d'une condition : `[class.myClass]="condition"`

**Note** : Vous pouvez trouver la solution de cette partie dans la branche `premier_component`