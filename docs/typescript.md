# TypeScript

Dans cette partie nous allons rappeler rapidement les éléments important de TypeScript nécessaires à l'élaboration d'une application Angular.

## Qu'est-ce que TypeScript

TypeScript est un langage de programmation libre et open source développé par Microsoft qui a pour but d'améliorer le JavaScript. Il s'agit d'un sur-ensemble syntaxique strict de JavaScript (c'est-à-dire que tout code JavaScript correct peut être utilisé avec TypeScript).

Le TypeScript ajoute donc des fonctionnalités au JavaScript classique, notamment : 

 - un typage fort
 - un modèle objet 

## La compilation

La particularité de TypeScript c'est qu'il n'est pas interprété directement par le navigateur ou par node pour les programmes back-end. En effet un programme TypeScript doit d'abord être compilé en JavaScript pour fonctionner. 

Si vous ne l'avez pas encore fait, vous pouvez installer `TypeScript` via la commande npm : 

```sh
npm install -g typescript
```

Grâce à cela vous pourrez ensuite exécuter le compilateur via la commande `tsc` suivi du nom du fichier à compiler. Le fichier compilé portera le même nom que le fichier source avec l'extension `.js`. On peut ensuite parfaitement exécuter le script `.js` en ligne de commande avec la commande `node`.

Exemple : 

```sh
tsc test.ts
node test.js
```

![typescript_s1](img/typescript_s1.png#center)

Angular met à disposition un serveur pour le développement qui va compiler le code à la volée ainsi qu'une commande pour la génération de l'application pour la production (html/css/js). Le développeur peut donc se concentrer sur la création de son application.

## Import

Pour importer des classes, fonctions ou variables depuis d'autres fichiers il faut utiliser le mot clé `import`. Les imports se font en début de fichier, par exemple :

```ts
// Importer une classe
import { Component } from '@angular/core'; 
// Importer toutes les classes, fonctions ou variables my-utils.ts dans le namespace utils
import * as utils from './my-utils'; 
```

## Le typage

Dans TypeScript il faut déclarer une variable avec le mot clé `let` suivi de son type, comme ceci :

```ts
let myVar: string;
```

Ici `myVar` sera de type string.

On peut aussi initialiser une valeur durant la déclaration :

```ts
let myVar: string = 'Bonjour';
```

**Note** : Si une variable est initialisée avec une valeur, le compilateur peut déterminer automatiquement son type :

```ts
let myVar = 'Bonjour'; // myVar sera automatiquement de type string
```

## Les types de base

 - `String` : chaîne de caractères
 - `Number` : nombre entier ou décimal
 - `Boolean` : booléen, true ou false
 - `Array` : tableau
 - `Tuple` : tableau avec des valeurs de types différents
 - `Enum` : énumération
 - `Unknown` : accepte n'importe quel type
 - `Any` : type non vérifié par le compilateur
 - `Void` : une fonction qui ne retourne pas de valeur


Exemples : 

```ts
let myString: string = 'Bonjour';
let myNumber: number = 10;
let myBool: boolean = true; 
let myArray: string[] = ['Gaston', 'Jean-Pierre', 'Marcel'];
let myTuple: [string, number] = ['Gaston', 10];

// Enum
enum Color {
    Red,
    Green,
    Blue,
}
let myColor: Color = Color.Red;

// Unknown
let myUnknown: unknown = 4;
myUnknown = "maybe a string instead";
myUnknown = false;

// Any
let myAny: any = o;
o.myFunction(); // Le compilateur ne vérifie pas l'éxistence de myFunction
o.myProperty; // Le compilateur ne vérifie pas l'éxistence de myProperty
```

## Le modèle objet

TypeScript offre un modèle objet comme on peut le rencontrer dans les langages comme `Java`, `C#` ou `PHP`.

Le langage introduit le mot-clé `class` pour créer une classe. Voici un exemple :

```ts
class MyClass { // Définition de la classe avec son nom
    public myProperty: string; // On défini une propriété avec son type

    constructor(myProperty: string) { // On peut définir un constructeur qui prend en paramètre myProperty
        this.myProperty = myProperty;    
    }

    print(): void { // On défini ici une méthode avec un type de retour
        console.log(this.myProperty);
    }
}
```

Une fois défini, nous pouvons instancier un nouvel objet à partir de notre classe. Chaque objet nouvellement créé prendra une place en mémoire.
Nous pouvons utiliser le nom de la classe comme `Type` objet.

Exemple :

```ts
// Pour utiliser notre classe MyClass
let myClass: MyClass = new MyClass('Salut le monde !');
myClass.print();
```

Nous pouvons également utiliser les interfaces pour définir un nouveau type d'objet ou pour obliger une classe à implementer certaines méthodes.
Pour définir une nouvelle interface il faut utiliser le mot clé `Interface`.

Pour définir un nouveau type objet : 

```ts
// Nouveau type d'objet
interface User {
    name: string;
    email: string;
    age: number;
}

// On peut définir un nouvel utilisateur avec le type User
let myUser: User = {
    name: 'Marcel',
    email: 'marcel@gmail.com',
    age: 45
};
```

Pour obliger une classe à implementer une méthode :

```ts
interface Human {
    public eat(): void; // Méthode obligatoire pour les humains
}

class Developer implements Human { // Developer implémente Human
    public name: string;
    public language: string;

    public eat(): void { // La fonction manger est obligatoire
        console.log('Developer eating');
    } 
}
```

