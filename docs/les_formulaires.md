# Les formulaires

Dans Angular les développeurs vont être amenés à manipuler un ou plusieurs formulaires.
Angular propose deux méthodes pour manipuler les formulaires : 

 - Les formulaires dirigés par le template : `template driven forms`
 - Les formulaires réactifs : `reactive forms`

Nous allons détailler ici les deux méthodes. Une des choses à savoir est que ces deux approches sont incompatibles entre elles. 
En effet, on ne peut pas utiliser à la fois un template driven form et un reactive form.

## Les template driven forms

Les template driven permettent d’utiliser la directive `ngModel` directement dans la vue HTML.
Elle permet de faire le lien entre le champ d'un formulaire et une propriété d'un `component`.

Tout d'abord pour utiliser les `template driven forms` il faut importer dans un module le `FormsModule` depuis `@angular/forms`.

Exemple : 

```ts
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FormsModule // Import du FormsModule
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
```

Ensuite il suffit de créer un formulaire dans la vue d'un component en utilisant la directive `ngModel` qui est maintenant disponible.

Exemple : 

```html
<form (submit)="submit()">
    <label>
        Nom : <input [(ngModel)]="book.bookName" />
    </label>
    <label>
        Auteur : <input [(ngModel)]="book.author" />
    </label>
    <label>
        Prix : <input [(ngModel)]="book.price" />
    </label>

    <button type="submit" [disabled]="checkForm()">Submit</button>
</form>
```

## Les reactive forms

Cette fois-ci pour utiliser les `reactive forms` il faut importer dans un module le `ReactiveFormsModule` depuis `@angular/forms`.

Exemple : 

```ts
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ReactiveFormsModule // Import du ReactiveFormsModule
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
```

Avec les `reactive forms` le développeur va créer son formulaire directement depuis le component `TypeScript`. 
Pour cela il peut utiliser un service pour l'aider : le `FormBuilder`.

Exemple :

```ts
import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-my-component',
    templateUrl: './my-component.component.html',
    styleUrls: ['./my-component.component.scss']
})
export class MyComponent {
    bookForm: FormGroup = this.fb.group({
        bookName: ['', Validators.compose([Validators.required])],
        author: ['', Validators.compose([Validators.required])],
        price: ['', Validators.compose([Validators.required])]
    });

    constructor(private fb: FormBuilder) { } // Ajout du service FormBuilder dans le component
}
```

Dans la vue il faut relier notre formulaire défini dans le component avec le formulaire HTML : 

```html
<form [formGroup]="bookForm" (ngSubmit)="submit()">
  <label>
        Nom : <input formControlName="bookName" />
    </label>
    <label>
        Auteur : <input formControlName="author" />
    </label>
    <label>
        Prix : <input formControlName="price" />
    </label>

    <button type="submit" [disabled]="!bookForm.valid">Submit</button>
</form>
```

On utilise : 

 - L'attribut formGroup sur la balise `form` pour lier le formulaire
 - L'évenement `ngSubmit` sur la balise `form` pour lier l'envoi du formulaire sur une méthode du component
 - L'attribut formControlName sur les balises `input` pour lier les champs
 - On désactive le bouton submit du formulaire si le formulaire est invalide 

## TP : Ajouter un formulaire de super héros

 1. Ajouter le module `FormsModule` dans le module `super-heroes`
 2. Ajouter un formulaire avec un champ qui a comme label `Nouveau super héro` au-dessus du bouton `Ajouter un nouveau héro`
 3. Ajouter une propriété `newHero` dans le component `SuperHeroesListComponent`
 4. Ajouter la fonction `submit()` dans le component `SuperHeroesListComponent`
 5. Lier le formulaire avec la propriété `newhero` et la fonction `submit()`
 6. Désactiver le bouton `Ajouter un nouveau héro` si `newhero` est vide
 7. La fonction `submit()` doit envoyer le nouveau super héro comme paramètre de sortie 
 8. Supprimer la propriété et 'input `newHeroAdded`
 9. Modifier la fonction `addNewHero` pour prendre en paramètre la sortie du formulaire et ainsi ajouter le nouveau super héro dans la liste

**Résultat attendu** : 

![les_formulaires_s1](img/7_les_formulaires_s1.png#center)

## Conclusion

Nous venons ici de voir comment manipuler les formulaires avec Angular et nous venons de faire un test avec la méthode `template driven`.