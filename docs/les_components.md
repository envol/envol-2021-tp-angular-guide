# Les components

Dans cette partie nous allons :

 - Apprendre à créer un nouveau component
 - Instancier le nouveau component dans l'application principale
 - Passer des paramètres d’entrée
 - Passer des paramètres de sortie

## Création d'un component Angular

Un component angular est composé de trois fichiers : 

 - Le fichier typescript qui représente le contrôleur : `my-component.component.ts`
 - Le fichier html qui représente la vue : `my-component.component.html`
 - Le fichier css qui représente le design ou style : `my-component.component.scss`

Un component est définis par une classe typescript sur laquelle on ajoute des métadonnées qu'on lui injecte via l'annotation `@Component`.

**Attention** : Toutes les classes, fonctions, tableaux, etc, que vous voulez utiliser dans un fichier `.ts` doivent d'abord être importés en tête du fichier avec le mot clé `import`. Donc pour pouvoir utiliser l'annotation `@Component` vous devez l'importer en haut de votre fichier `.ts`, comme ceci : 

```ts
import { Component } from '@angular/core';
```

Pour un nouveau component, les métadonnées que vous devez spécifier sont : 

 - `selector` : Le nom de la balise html qui servira à appeler une nouvelle instance du component
 - `templateUrl` : Le fichier qui contient la vue html du component
 - `styleUrls` : Un tableau qui contient le ou les fichiers CSS à appliquer à la vue HTML

**Exemple** : 

```ts
import { Component } from '@angular/core';

@Component({
    selector: 'app-my-component',
    templateUrl: './my-component.component.html',
    styleUrls: ['./my-component.component.scss']
})
export class MyComponent {

}
```

Pour pouvoir utiliser un component vous devez également le déclarer dans le module principal.
Par exemple pour déclarer `MyComponent` dans le module `AppModule` vous devez l'importer puis l'ajouter dans le tableau `declarations`, comme ceci : 

```ts
// app.module.ts
...
import { MyComponent } from './my-component.component';

@NgModule({
    declarations: [
        AppComponent,
        MyComponent // Mon nouveau component
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
```

Une fois que vous avez définis un component vous pouvez l'instancier une ou plusieurs fois depuis la vue.
Chaque version instanciée d'un component est unique et occupera de la place en mémoire.

Vous pouvez donc instancier le component MyComponent dans votre vue html, comme ceci :

```html
<my-component></my-component>
```

## TP : Création d'un nouveau component

 1. Ajouter un nouveau component `SuperHeroesListComponent` dans votre répertoire principal 
 2. Ajouter un texte dans votre nouveau component : `Ceci est mon premier component`
 3. Déclarer le nouveau component dans le module principal `AppModuleComponent`
 4. Appeler ce nouveau component depuis le template d'AppComponent pour le visualiser

**Résultat attendu** : 

![creation_component_s1](img/4_creation_component_s1.png#center)

## Les paramètres d'entrée

Un component Angular peut prendre de 0 à plusieurs paramètres d'entrée. Si un paramètre d'entrée est défini, il faudra spécifier une valeur lors de l'instanciation.
Pour définir un paramètre d'entrée il faut utiliser l'annotation `@Input`, comme ceci : 

```ts
@Input() myParam: string;
```

Il faut donc définir le nom du paramètre, son type et la valeur par défaut si le paramètre n'est pas renseigné à l'appel. 

**Exemple** : 

```ts
// my-component.component.ts
import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-my-component',
    templateUrl: './my-component.component.html',
    styleUrls: ['./my-component.component.scss']
})
export class MyComponent {
    @Input() myParam: string = '';
}
```

**Note** : Pour utiliser l'annotation `@Input`, il ne faut pas oublier de l'importer en tête du fichier `.ts`

Vous pouvez ensuite utiliser la propriété `myParam` normalement, par exemple en l'affichant dans votre vue : `{{ myParam }}`

Pour spécifier une valeur d'un paramètre d'entrée il faut utiliser le `property binding` avec les doubles crochets : `[myParam]="value"`

Vous pouvez donc instancier le component `MyComponent` dans votre vue html avec le paramètre d'entrée, comme ceci :

```html
<my-component [myParam]="value"></my-component>
```

**Note** : La propriété `value` doit être présent dans le component `AppComponent` pour qu'elle soit liée au component instancié.

## TP : Passer la liste de super héros en Input

 1. Ajouter un paramètre d'entrée `superheroes` de type tableau de string (`string[]`) à votre component `SuperHeroesListComponent` avec comme valeur par défaut un tableau vide
 2. Déplacer la liste `ul - li` d'affichage des héros dans la vue du component `SuperHeroesListComponent`
 3. Dans le component `AppComponent` remplacer la liste `ul - li` par l'appel du component `SuperHeroesListComponent` en passant en paramètre d'entrée la liste de super héros du component `AppComponent`

**Note** : N'oubliez pas de déplacer également le style css du component `AppComponent` vers le component `SuperHeroesListComponent`.

**Résultat attendu** : 

![creation_component_s2](img/4_creation_component_s2.png#center)

## Les paramètres de sortie

Un component Angular peut prendre de 0 à plusieurs paramètres de sortie. Un paramètre de sortie renvoie toujours un objet de type `EventEmitter`. Le développeur doit également définir le type de valeur qui sera renvoyé dans l'évenement.

Pour définir un paramètre de sortie il faut utiliser l'annotation `@Output` et le type `EventEmitter`, comme ceci : 

```ts
@Output() myAction: EventEmitter<string> = new EventEmitter();
```

**Exemple** : 

```ts
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-my-component',
    templateUrl: './my-component.component.html',
    styleUrls: ['./my-component.component.scss']
})
export class MyComponent {
    @Input() myParam: string = '';
    @Output() myAction: EventEmitter<string> = new EventEmitter();
}
```

**Note** : Pour instancier un nouvel objet de type `EventEmitter` il faut utiliser le mot clé `new`.

Nous pouvons donc ensuite définir dans notre component à quel endroit l'évenement de sortie sera déclenché. Par exemple sur le clic d'un bouton. Il faudra alors appeler la méthode `emit` de l'objet `EventEmitter` pour déclencher l'évenement de sortie, par exemple comme ceci : 

```html
<button type="button" (click)="myAction.emit('Paramètre de sortie')">Action</button>
```

**Note** : Pour appeler une méthode d'un objet il faut utiliser le `.` : `objet`.`methode()`.

Nous pouvons ensuite lier le paramètre de sortie `myAction` sur une action lorsque le component est instancié, comme ceci : 

```html
<my-component (myAction)="action($event)"></my-component>
```

## TP : Déplacer le bouton d'ajout

 1. Ajouter un paramètre de sortie `addNewHero` au composant `SuperHeroesListComponent` de type `EventEmitter` et sans valeur de retour
 2. Ajouter un paramètre d'entrée `newHeroAdded` au composant `SuperHeroesListComponent` de type `boolean` avec comme valeur par défaut `false`
 3. Déplacer le bouton `Ajouter un nouveau héro` dans la vue du component `SuperHeroesListComponent`
 4. Au clic du bouton, déclencher le paramètre de sortie `addNewHero`
 5. Dans la vue `AppComponent` à l'appel du component `SuperHeroesListComponent` lier la propriété `newHeroAdded` au paramètre d'entrée du même nom
 6. Dans la vue `AppComponent` à l'appel du component `SuperHeroesListComponent` lier l'évenement de sortie `addNewHero` à la fonction du même nom

**Note** : Pour spécifier un paramètre de sortie sans valeur de retour, utiliser la notation `{}` : `EventEmitter<{}> = new EventEmitter();`

**Résultat attendu** : 

![creation_component_s3](img/4_creation_component_s3.png#center)

## Conclusion

Dans cette partie vous avez appris à fabriquer votre premier component Angular, notamment : 

 - Définir les métadonnées d'un component : `selector`, `template`, `style`
 - Instancier un nouveau component à partir de son sélecteur : `<my-component></my-component>`
 - Définir des paramètres d'entrée : `@Input()`
 - Lier un paramètre d'entrée avec une variable : `<my-component [myParam]="value"></my-component>`
 - Définir des paramètres de sortie : `@Output()`
 - Lier une action à un paramètre de sortie : `<my-component (myAction)="action($event)"></my-component>`