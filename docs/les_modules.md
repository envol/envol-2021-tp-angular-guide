# Les modules

Les modules permettent de regrouper un ensemble de components, de services, de directives, etc...
Le développeur choisira de créer des modules pour découper son application par fonctionnalités. Ce qui permettra une meilleure organisation du code.

## NgModule

Pour ajouter un nouveau module vous devez ajouter un nouveau répertoire puis créer une classe `TypeScript` sur laquelle on ajoute des métadonnées qu'on lui injecte via l'annotation `@NgModule`.

Pour un nouveau module, les métadonnées que vous devez spécifier sont :

 - `declarations` : Un tableau qui contient les components du module
 - `imports` : Un tableau qui contient les modules ou components externes à importer dans le nouveau module
 - `exports` : Un tableau qui contient les components ou module qui peuvent être utilisé ailleurs dans l'application
 - `providers` : Les services définis dans ce nouveau module

Exemple `my-feature/my-feature.module.ts` : 

```ts
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyFeatureComponent } from './my-feature.component';
import { MyFeatureService } from './my-feature.service';

@NgModule({
    declarations: [
        MyFeatureComponent
    ],
    imports: [
        CommonModule // Import obligatoire pour utiliser les fonctionnalités de base
    ],
    providers: [
        MyFeatureService
    ]
})
export class MyFeatureModule { }
```

**Attention** : Le nouveau module doit importer `CommonModule` depuis `@angular/common` pour pouvoir utiliser les fonctionnalités de base telles que `*ngIf` ou `*ngFor`.

Pour que le nouveau module soit utilisable, il doit être déclaré dans les imports du module principal de l'application : `app.module.ts`.

Exemple `app.module.ts` :

```ts
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MyFeatureModule // Déclaration du nouveau module
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
```

Le module `MyFeatureModule` est maintenant déclaré et utilisable.

## Routing de module

Vous pouvez déclarer un nouveau fichier de routing pour votre nouveau module.
Pour gérer le routing au sein de notre nouveau module, nous créons un fichier de routing dans notre nouveau module. 

Exemple `my-feature/my-feature-routing.module.ts` : 

```ts
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SuperHeroesComponent } from './super-heroes.component';

const routes: Routes = [
    { path: 'my-feature', component: MyFeatureComponent }
];

@NgModule({
    // Attention il faut appeler la méthode forChild pour déclarer un fichier de routing supplémentaire
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MyFeatureRoutingModule { }
```

Il faut ensuite déclarer ce nouveau fichier de routing dans le nouveau module.

Exemple `my-feature/my-feature.module.ts` : 

```ts
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyFeatureComponent } from './my-feature.component';
import { MyFeatureService } from './my-feature.service';
import { MyFeatureRoutingModule } from './my-feature-routing.module.ts';

@NgModule({
    declarations: [
        MyFeatureComponent
    ],
    imports: [
        CommonModule,
        MyFeatureRoutingModule // Déclaration du nouveau fichier de routing
    ],
    providers: [ MyFeatureService ]
})
export class MyFeatureModule { }
```

## TP : Créer un module pour la fonctionnalité super héros

 1. Créer un répertoire `super-heroes` puis créer le module `super-heroes.module.ts`
 2. Déclarer le module `SuperHeroesModule` dans le module principal `AppModule`
 3. Déplacer les components correspondant aux fonctionnalités de super héros dans le nouveau module
 4. Ajouter un nouveau fichier de routing pour le nouveau module
 5. Déplacer la route `super-heroes` dans le fichier de routing du module `SuperHeroesModule`

## Conclusion

Nous avons appris à nous servir des modules. Nous avons mieux organisé notre code mais le résultat n'a pas changé.