# Installation de l'environnement de développement

Dans cette partie nous allons détailler les différents outils nécessaires au développement de notre application web Angular.

## IDE

Vous aurez besoin d'un éditeur de code afin de programmer notre application. Nous conseillons `Visual studio code` qui est un IDE multi plateforme (Windows, Linux, MacOS).
VSCode peut être utilisé avec une variété de langages de programmation, notamment Java, JavaScript, Go, Node.js et C++.
Il est basé sur de l'open source et est utilisable librement. 

Si vous ne l'avez pas encore installé rendez-vous sur [https://code.visualstudio.com/](https://code.visualstudio.com/).

## Docker

Il vous faudra également `Docker` qui est une plateforme permettant de lancer certaines applications dans des conteneurs logiciels indépendamment du système d'exploitation utilisé sur votre machine.

Ici il va nous permettre de simplifier l'installation de l'environnement de développement (dépendances, config, serveur de dev...).

Il faut donc vous assurer que les commandes docker et docker-compose soient disponibles sur votre ordinateur.

```sh
docker
docker-compose
```

Si vous ne les avez pas encore installées rendez-vous sur [https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/)

**Attention** : Si vous êtes sur Linux, vous devez installer docker-compose manuellement [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

## Git

Il faudra également avoir `Git` qui est un logiciel de gestion de versions décentralisé. Il va nous permettre de récupérer le dépôt du code source de notre TP.

La commande git doit donc être disponible sinon rendez-vous sur [https://git-scm.com/downloads](https://git-scm.com/downloads)

## Make

Pour pouvoir exécuter les actions de base (install, start, stop, restart, build, tests, ...), nous utiliserons la commande `make` avec un fichier `Makefile` à la racine du projet.
Vous devez donc disposer de la commande make ou l'installer.

## NPM et TypeScript

Assurez-vous d'avoir installé la commande `npm` pour pouvoir installer `TypeScript` sur votre ordinateur. Pour installer `TypeScript` :

```sh
npm install -g typescript 
```