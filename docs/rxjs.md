# RxJS

Dans cette partie nous allons parler du module `RxJS` qui est utilisé dans Angular pour faire de la programmation reactive.
Il va nous être utile notamment pour la gestion des requêtes asynchrones (HTTP) dans notre application.

## Le pattern Observer

Dans Angular nous allons utiliser la classe `Observable` du module `RxJS`. Le principe est que l'on définit des observables et des observateurs. Les observables vont émettre des événements qui seront interceptés par les observateurs.

Un `Observable` va agir de manière similaire à une promesse en JavaScript. Une fonction de callback, l'observateur, sera déclenchée à chaque nouvel état de l'observable.

Exemple : 

```ts
// Import la classe Observable
import { Observable, of } from 'rxjs';

// Fabrique un objet Observable avec une valeur
const myObservable: Observable<string> = of('Bonjour ENVOL');

// On souscrit à l'observable avec comme paramètre la fonction de callback à exécuter
myObservable.subscribe((value) => { console.log(value) });
```

Exemple dans Angular avec `HttpClient`

```ts
// La fonction get retourne un observable sur lequel on souscrit
// La fonction de callback sera exécutée quand le serveur aura renvoyé le résultat

this.httpClient.get<string>('mon_url').subscribe(value => {
    console.log(value);
});
```

## Travailler sur les observables

Il est possible avec `RxJS` de modifier la valeur de l'observable avant d'y souscrire.
Pour ce faire il faut utiliser la fonction `pipe` sur l'Observable.
Vous pouvez ensuite chainer des fonctions de modification ou de filtre sur le flux à la manière d'un pipe dans Linux bash.

Exemple :

```ts
import { filter } from 'rxjs/operators';

// Récupère des articles depuis le serveur et filtre sur l'auteur
this.httpClient.get<Article[]>('mes_articles_url').pipe(
    filter(article => article.author === 'Jean Dupont')
).subscribe(value => {
    console.log(value);
});
```