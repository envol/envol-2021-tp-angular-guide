# TP Communes

Maintenant que vous vous êtes familiarisé avec Angular, nous allons maintenant développer un module complet pour interroger des web services publiques distants et disponibles sur [https://api.gouv.fr/](https://api.gouv.fr/).

Ces web services vont nous permettre de récupérer des informations sur les communes et départements en France.
Nous afficherons ensuite les informations sous forme de graphs en utilisant un module Angular tierce.

## TP : Création du nouveau module

 1. Créer un nouveau module `commune`
 2. Créer un nouveau component `commune.component.ts` qui sera le point d'entrée de votre nouveau module
 3. Créer un nouveau fichier de routing pour naviguer vers votre nouveau module `commune-routing.module.ts` avec comme path `commune`
 4. Importer votre nouveau module dans le module principal
 5. Dans la barre de menu, créer un lien pour que l'utilisateur puisse se rendre dans le module région

**Note** : Vous pouvez utiliser le font-awesome `fas fa-map-signs` comme icône pour le lien vers région.

**Résultat attendu** : 

![commune_s1](img/commune_s1.png#center)

## TP : Chargement des départements

Dans cette partie nous allons faire appel aux web services qui permettent de récupérer des informations sur les départements et sur les communes.

Les deux webs services utilisés sont : 

 - **https://geo.api.gouv.fr/departements** : Pour lister tous les départements en France
 - **https://geo.api.gouv.fr/departements/{code}/communes** : Pour lister les communes d'un département. **Attention** {code} doit être remplacé par le numéro du département

Le web service qui liste les départements retourne un tableau d'objet département, exemple : 

```js
[
    {
        "nom": "Ain",
        "code": "01",
        "codeRegion": "84"
    },
    {
        "nom": "Aisne",
        "code": "02",
        "codeRegion": "32"
    }
]
```

Le web service qui liste les communes retourne un tableau d'objet commune, exemple : 

```js
[
    {
        "nom": "Aix-en-Provence",
        "code": "13001",
        "codeDepartement": "13",
        "codeRegion": "93",
        "codesPostaux": [
            "13090",
            "13100",
            "13290",
            "13080",
            "13540",
            "13122"
        ],
        "population": 143006
    },
    {
        "nom": "Allauch",
        "code": "13002",
        "codeDepartement": "13",
        "codeRegion": "93",
        "codesPostaux": [
            "13190"
        ],
        "population": 21228
    }
]
```

 1. Créer un fichier `departement.model.ts` pour définir l'objet `departement` (`Interface`)
 2. Créer un fichier `commune.model.ts` pour définir l'objet `commune` (`Interface`)
 3. Dans le component `commune.component.ts` faites appel au module `HttpClient`
 4. Ajouter une méthode `loadDepartements` qui va charger la liste des départements (utiliser l'URL adéquate)
 5. Dans le component `commune.component.ts` ajouter les propriétés `departements`, `departementsIsLoading` et `departementsIsLoaded` pour gérer le chargement

## TP : Création d'un tableau de département

 1. Créer un nouveau component `departement-table.component.ts`
 2. Ce component doit prendre comme paramètres d'entrée la liste des départements `departements` et les deux flags `departementsIsLoading` et `departementsIsLoaded`
 3. Ce component doit aussi prendre un paramètre de sortie `loadDepartements`
 4. Dans le HTML, ajouter un bouton `Charger les départements` qui doit envoyer un signal au paramètre de sortie
 5. Le bouton `Charger les départements` ne doit être visible que si `departementsIsLoaded` est `false`
 6. Dans le HTML, ajouter un tableau bootstrap pour afficher les départements (boucler grâce à `*ngFor`)
 7. Le tableau ne doit être visible que si `departementsIsLoaded` est `true`
 8. Ajouter un spinner durant le chargement des départements en haut du component 
 5. Déclarer votre nouveau component `departement-table.component.ts` dans le module `commune`

**Résultat attendu** : 

![commune_s2](img/commune_s2.png#center)

## TP : Amélioration du design avec une pagination

Nous allons ici utiliser un component qui fait partie d'une bibliothèque tierce : `ngx-boostrap`. Nous allons utiliser le component `pagination` pour ajouter une pagination sur notre tableau de départements. Par défaut la pagination se fait de 10 en 10. Il prend en paramètre le nombre total du tableau à paginer `totalItems` ainsi qu'une variable `currentPage` qui nous permettra de connaître la page actuelle.

Nous allons également utiliser la fonction JavaScript `slice` qui permet de découper un tableau. Cette fonction prend deux paramètres : la position du début et la position de fin.

 1. Importer le module `FormsModule` dans le module `commune`
 2. Importer le module de pagination de l'extension `ngx-boostrap` comme ceci `import { PaginationModule } from 'ngx-bootstrap/pagination';` dans le module `commune`
 3. Ajouter une propriété `currentPage` de type `number` dans le component `departement-table.component.ts`
 4. Dans la vue `departement-table.component.ts` faites appel au component `<pagination [totalItems]="departements.length" [(ngModel)]="currentPage"></pagination>`
 5. Utiliser la méthode `slice` sur le tableau des départements pour le découper de 10 en 10

**Résultat attendu** : 

![commune_s3](img/commune_s3.png#center)

## TP : Ajout d'un champ de recherche de département

En JavaScript vous pouvez filtrer un tableau avec la fonction `filter` :

```ts
const articles = [
    { 
        name: 'chips',
        price: 10
    },
    {
        name: 'ketchup',
        price: 15
    }
];

const filteredArticles = articles.filter(article => article.price < 12);
// Retourne [{ name:'chips', price:10 }]
```

En JavaScript vous pouvez utiliser la fonction `includes` pour voir si une sous chaîne est présente dans une `string`.
Vous pouvez également appeler la fonction `toLowerCase()` sur une `string` pour transformer toutes les majuscules en minuscules.

 1. Ajouter un champ `input` de recherche au-dessus du tableau des départements
 2. Lier une nouvelle propriété `search` du component `departement-table.component.ts` au formulaire de recherche
 3. Ajouter un filtre sur le tableau des départements en fonction de la valeur de `search`

**Résultat attendu** : 

![commune_s4](img/commune_s4.png#center)

## TP : Récupérer les communes par département

 1. Dans le component `commune.component.ts` ajouter les propriété `communes`, `communesIsLoading` et `communesIsLoaded` pour gérer le chargement
 2. Ajouter la fonction `loadCommunes` qui prend un paramètre le `codeDepartement`
 3. Faites appel au web service pour récupérer les communes du département correspondant au `codeDepartement`
 4. Ajouter un component `commune-table.component.ts`
 5. Développer le component pour afficher les communes de la même façon que le component `departement-table.component.ts`
 6. Ajouter une colonne dans la table des départements qui doit contenir une icône loupe
 7. Ajouter un paramètre de sortie `loadCommunes` qui doit renvoyer le code département dans le component `departement-table.component.ts` 
 8. Lier l'icône loupe avec le paramètre de sortie `loadCommunes`
 9. Appeler le component `commune-table.component.ts` depuis la vue du component principal `commune.component.ts`

**Résultat attendu** : 

![commune_s5](img/commune_s5.png#center)

## TP : Ajouter une notification au chargement

 1. Importer le module `import { ToastrModule } from 'ngx-toastr';` dans le module principal `app.module.ts`
 2. Ajouter le module dans la liste des imports comme ceci `ToastrModule.forRoot()`
 3. Appeler le service `private toastr: ToastrService` dans le component `commune.component.ts` du module `commune`
 4. Appeler la méthode `success` à chaque fois qu'un chargement d'un web service a fonctionné `this.toastr.success('Liste des départements chargée');`

**Résultat attendu** : 

![commune_s6](img/commune_s6.png#center)

## TP : Visualisation avec un graphique

Pour créer un graph nous allons utiliser un component qui provient de la bibliothèque `NgxChartsModule`, comme ceci :

```html
<ngx-charts-bar-vertical 
    [view]="[1000,400]"
    [results]="communesForGraph"
    [xAxisLabel]="'Villes'"
    [legendTitle]="'Population par ville'"
    [yAxisLabel]="'Population'"
    [legend]="true"
    [showXAxisLabel]="true"
    [showYAxisLabel]="true"
    [xAxis]="true"
    [yAxis]="true"
    [gradient]="false">
</ngx-charts-bar-vertical>
```

 1. Ajouter le module `NgxChartsModule` qui provient de `@swimlane/ngx-charts` dans la liste des imports du module `commune`
 2. Ajouter un nouveau component `commune-graph.component.ts`
 3. Ajouter une nouvelle propriété `communesForGraph` dans le component `commune`
 4. Au chargement des communes, affecter un tableau d'objet comprenant 2 propriétés : `name` = nom de la commune et `value` = population pour les villes dont la population est supérieur à 10 000 habitants
 5. Ajouter 3 inputs pour le component `commune-graph.component.ts` : `communesForGraph`, `communesIsLoading`, `communesIsLoaded`
 6. Dans la vue `commune-graph.component.html`, appeler le component `ngx-charts-bar-vertical`. Appeler ce component seulement si `communesIsLoaded` est vrai
 7. Dans la vue `commune.component.ts` après le tableau des communes, ajouter l'appel du component `commune-graph.component.ts`

**Résultat attendu** : 

![commune_s7](img/commune_s7.png#center)

## TP : Pour aller plus loin

Pour aller plus loin dans ce TP vous pouvez ajouter des fonctionnalités comme : 

 - Donner la possibilité aux utilisateurs de changer la taille des villes affichées dans le graph. Par exemple afficher les villes avec une population comprise entre 5000 et 10000 habitants.
 - Trier le tableau des communes par taille de population
 - Ajouter une boîte avec 3 onglets (départements, communes, graph)
 - Ajouter la gestion du menu quand le site est affiché sur smartphone ou tablette (responsive design)
 