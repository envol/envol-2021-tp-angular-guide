# Les services

Dans cette partie nous allons :

 - Expliquer la notion de service dans Angular
 - Injecter le service `HttpClient` dans notre component
 - Utiliser une requête HTTP pour récupérer des données

## Notion de service dans Angular

Un service est un objet dont l'instanciation est gérée par Angular. Cet objet est unique dans toute l'application et il peut être injecté dans n'importe lequel de vos components ou bien encore dans un autre service. 

Un service permet donc de : 

 - Partager des fonctionnalités entre plusieurs components
 - Partager des propriétés entre plusieurs components
 - Communiquer entres les components
 - Développer les fonctionnalités techniques de l'application

Angular propose plusieurs services que vous pouvez utiliser dans vos applications comme : 

 - `HttpClient` : Permet de lancer des requêtes HTTP vers un serveur (GET, POST, PUT, DELETE)
 - `FormBuilder` : Permet de faciliter la création d'un objet formulaire
 - `Router` : Permet de gérer la navigation de l'utilisateur dans l'application

Vous pouvez bien évidement créer votre propre service et demander à Angular de gérer son instanciation. Ce service sera unique dans toute l'application et vous pourrez l'injecter dans vos components ou services. 

Pour créer un service il suffit de créer une classe précédée de l'annotation `@Injectable`.

```ts
import { Injectable } from '@angular/core';

@Injectable()
export class AppService {
    myParams: string[] = [];

    myFunction(param: string): void {
        this.myParams.push(param);
    }
}
```

Puis il faut informer votre application qu'un nouveau service est disponible. Pour cela vous devez le déclarer dans le tableau `providers` du module `AppModule`, comme ceci : 

```ts
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AppService } from './app.service';

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule
    ],
    providers: [AppService], // Déclaration du nouveau service
    bootstrap: [AppComponent]
})
export class AppModule { }
```

Vous pouvez maintenant l'injecter dans un component pour l'utiliser en ajoutant un paramètre au constructeur. Ce paramètre comprend un nom suivi du type du service à injecter, par exemple : 

```ts
import { Component } from '@angular/core';

import { AppService } from './app.service';

@Component({
    selector: 'app-my-component',
    templateUrl: './my-component.component.html',
    styleUrls: ['./my-component.component.scss']
})
export class MyComponent {
    constructor(private appService: AppService) { } // Injection du service AppService
}
```

Pour résumer, au démarrage de l'application Angular va instancier un nouvel objet de type `AppService` puis va l'injecter automatiquement dans les components ou services qui en ont fait la demande via leurs constructeurs. Cet objet est unique et est partagé dans toute l'application.

## TP : Injecter le service `HttpClient` dans notre component

Angular fournit un service `HttpClient` qui permet de faire des requêtes `http` asynchrones. Ceci est utile pour poster ou recevoir des données.
Pour avoir accès à ce service, vous devez charger un module fournit dans le framework Angular, le `HttpClientModule`.

 1. Ouvrez le fichier contenant le `AppModule`
 2. En haut de ce fichier, importez le `HttpClientModule` comme ceci : `import { HttpClientModule } from '@angular/common/http';`
 3. Dans le tableau des `imports` ajouter le `HttpClientModule`
 4. Enfin dans le component `AppComponent` injecter le service `HttpClient`

## Le service `HttpClient`

Le service `HttpClient` permet de lancer une requête HTTP. Pour utiliser ce service vous devez appeler la méthode qui correspond à la méthode HTTP utilisée.
Par exemple pour une requête `get` vous utiliserez la méthode `get()`. Cette méthode prend en paramètre l'URL appelée et renvoie un `Observable`. Nous pouvons souscrire à cet `Observable` pour exécuter une fonction de callback une fois que la requête sera résolue.

**Attention** : Il faut spécifier le type de valeur retourné par la requête en utilisant les chevrons après le nom de la méthode.

Exemple : 

```ts
this.httpClient.get<string[]>('http://my-url.com').subscribe(data => {
    console.log(data);
});
```

## TP : Utiliser une requête HTTP pour récupérer des données

 1. Créer un fichier `superheroes.json` dans le dossier `client/src/assets`
 2. Copier la liste des super-héros dans le fichier précédemment créé. Attention un JSON valide n'accepte que les double quotes `"` pour les strings
 3. Dans `AppComponent`, initialiser la liste des super-héros avec un tableau vide `[]`
 4. Dans `AppComponent`, implémenter l'interface `OnInit`. N'oubliez pas de l'importer depuis le package `@angular/core`
 5. Ajouter la fonction `ngOnInit()`. Cette fonction sera exécutée à la création du component. Donc au démarrage de l'application
 6. Lancer une requête HTTP pour lire votre fichier avec l'url `assets/superheroes.json`
 7. Abonnez-vous à l'Observable puis affecter le résultat à la variable `superheroes` du component `AppComponent`

L'application va donc démarrer, lancer une requête HTTP pour récupérer la valeur du fichier `superheroes.json` puis affecter la valeur du résultat à la propriété `superheroes` du component `AppComponent`.

**Résultat attendu** : 

![creation_component_s3](img/4_creation_component_s3.png#center)

## TP : Ajouter un spinner de chargement

Pour afficher un spinner font-awesome, il faut utiliser le code suivant : 

```html
<div>
    <span class="fas fa-circle-notch fa-spin fa-3x"></span>
    <span class="sr-only">Loading...</span>
</div>
```

 1. Ajouter deux propriétés à votre component `AppComponent` : `superheroesIsLoading` et `superheroesIsLoaded` avec comme valeur `false`
 2. Passer ces deux propriétés comme valeur d'`Input` du component `SuperHeroesListComponent`
 3. Au chargement de l'application avant de lancer la requête HTTP affecter la valeur `true` à `superheroesIsLoading`
 4. Dans la fonction de callback de l'Observable, affecter la valeur `false` à `superheroesIsLoading` et `true` pour `superheroesIsLoaded`
 5. Dans `SuperHeroesListComponent`, afficher un spinner uniquement si `superheroesIsLoading` est vrai
 6. Dans `SuperHeroesListComponent`, afficher la liste de super-héros et le bouton seulement si `superheroesIsLoaded` est vrai
 7. Importer la fonction `delay` depuis `rxjs/operators`
 8. Pour ajouter un délai de chargement à la requête HTTP, ajouter `.pipe(delay(1000))` après la fonction `get`

L'application va donc afficher un spinner de chargement jusqu'au chargement des données distantes, içi le fichier JSON.

![les_services_s1](img/5_les_services_s1.png#center)

## Conclusion

Dans cette partie vous avez appris à : 

 - créer un service
 - déclarer un service
 - utiliser un service dans un component
 - utiliser `HttpClient` pour exécuter une requête
 - charger des données distantes avec un Observable et une fonction de callback
 - afficher un spinner pour les temps de chargement